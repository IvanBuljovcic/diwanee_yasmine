require 'compass-normalize'
require 'compass/import-once/activate'
require 'rgbapng'
# Require any additional compass plugins here.


# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "assets/scss"
images_dir = "assets/img"
javascripts_dir = "assets/js"
fonts_dir = "assets/fonts"

output_style = :compressed

enviroment = :development

relative_assets = true

# Copies the style.css from /css to the root folder when compiling is finished

# require 'fileutils'
# on_stylesheet_saved do |file|
#   if File.exists?(file) && File.basename(file) == "style.css"
#     puts "Moving: #{file}"
#     FileUtils.cp(file, File.dirname(file) + "/../" + File.basename(file))
#   end
# end