// Load functions
document.addEventListener('DOMContentLoaded', function () {
	gutterResize();
});

// On window resize
$(window).resize(function() {
	gutterResize();
});



// Navigation toggle
var $menuTrigger = document.querySelector(".menu-trigger"),
	$navigation = document.querySelector(".site-navigation .navigation-list");

$menuTrigger.addEventListener('click', function(e) {
	e.preventDefault();

	if ($navigation.classList.contains('desktop')) {
		$navigation.classList.remove('desktop');
	}
	else if ($navigation.classList.contains('desktop') != true) {
		$navigation.classList.add('desktop');
	}
});

// Pagination
	// Resize items to same width as their height
		// jQuery; Rewrite to pure JavaScript!
var $navItem = $('.pagination ul li'),
	height = $navItem.height();
	
	$navItem.width(height + "px");

// Gutter height
	// Set gutter height to container height
		// Done using jQuery; Rewrite to pure JavaScript!
function gutterResize () {
	var $gutter = $('.gutter');
	
	$.each($gutter, function() {
		var $this = $(this),
			$container = $this.parent('.text-wrapper'),
			parentHeight = $container.height();

		$this.height(parentHeight + 'px');
	});
}