// Configuration
var width = 720,
    height = 400,
    animationSpeed = 1000,
    pause = 3000,
    currentSlide = 1;

// DOM cache
var $slider = $('#slider'),
    $slideContainer = $slider.children('.slides'),
    $slides = $slideContainer.children('.slide');

var $next = $('.next');

    $slides.first().clone().appendTo($slideContainer);

var $slides = $slideContainer.find('.slide'),
    numberOfSlides = $slides.length;

// setInterval
  // animate margin-left
  // if it's the last slide, go to position 1 (0px)
var interval;

var $bullet = $('.bullet');
for (var i = 0; i < numberOfSlides; i++) {
  $bullet.append('<span id="'+i+'">'+i+'</span>');
}

$bullet.children('span').on('click', function () {
  var $id = $(this).attr('id');

  currentSLide = $id;
  console.log($id);
  console.log(currentSlide);
});

function startSlider () { 
  interval = setInterval(animate(), pause);
}

function animate () {
  $slideContainer.animate({ 'margin-left': '-='+ width }, animationSpeed, function () {
    currentSlide++;
    if(currentSlide === $slides.length){
      currentSlide = 1;
      $slideContainer.css('margin-left',0);
    }
  });
}

function stopSlider () {
  clearInterval(interval);
}

$next.on('click',animate);

$slider.on('mouseenter', stopSlider).on('mouseleave',startSlider);


startSlider();